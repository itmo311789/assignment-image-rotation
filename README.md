# Assignment: Image rotation
---
Image rotation

# Include sorting order
There is absolutely no point in having includes to be mandatory sorted in alphabetical order. If google or clang team think that they are being all smart pants about their coding style it's ok, but it's just another level of complication and another wall. Any barely complex project probably has precompiled headers and/or some sort of auto generated code. In either case you have to include this header first or last despite of their name. Ok, then you put this headers into a white list of headers even if there is one. And then on different platform you go to another build system and everything starts from scratch. Wouldn't it better to just let people do whatever it takes to write and test software instead of making them deal with include order and all the crap that it leads to?

# Linux walls
And that is only one wall that linux fan boys put on the way of programmers. For some unknown reason they think that copying and setting memory is incredibly dangerous (Probably nobody invented canaries yet, right?). So now you are just not allowed to use memcpy or any other compiler intrinsic that is not safe enough for you to use, dummy. That's kinda offensive, isn't it? But hey, in vast majority of the languages you are just not allowed to manage your memory.

# Nobody expect linux to even work
Have you ever heard of games on linux? Yes, I also haven't. That's because these multi-billion dollar companies don't want to give out their sources, what a jerks they are! And then why they can't give you the binaries if they have them? (for example bf2042 has linux binaries for the server, which literally the same source code but a different build) Then almost nobody will even by able to run this binaries, because every single distro rolls it's own barely working crappy API for every little thing. You can't just test that your code works with x11 on one distro, and then expect it to run at all on the other one.
