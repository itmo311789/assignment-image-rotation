#pragma once
#include <assert.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ZeroMemory(struct_arg) MemsetSuperSafe(&struct_arg, 0, sizeof(struct_arg))
#define memcpy_s(dest, dest_size, src, copy_range) assert(dest_size <= copy_range); MemcpySuperSafe(dest, src, copy_range)


void MemsetSuperSafe(void* dest, int value, size_t size);
void MemcpySuperSafe(void* dest, void* src, size_t copy_range);

