#pragma once
#include <stddef.h>
#include <stdint.h>

typedef enum {
	PF_NONE   = 0,
	
	PF_R8G8B8 = 1,
	// Add new pixel formats here. Don't forget to change GetPixelFormatSize!
	// For example you might need to work with hdr images or images with alpha channal.
	// Such images will have different pixel strides.

	PF_COUNT  = 2
} PixelFormat;

typedef struct {
	uint32_t width;
	uint32_t height;

	PixelFormat pixel_format;

	void* pixels;
} Image;

size_t GetPixelFormatSize(PixelFormat pixel_format);
size_t CalculateImageSize(const Image* image);
void FreeImage(Image* image);
