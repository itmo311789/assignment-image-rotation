#pragma once
#include "Image.h"

#include <stdbool.h>
#include <stdio.h>

bool ReadBMPImage(FILE* file, Image* image);
bool WriteBMPImage(FILE* file, Image* image);
