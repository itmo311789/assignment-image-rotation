#include "BmpReadWrite.h"

#include "Misc.h"

#if defined(_MSC_VER)
#pragma pack(push, 1)
#define ALIGN_1
#else
#define ALIGN_1 __attribute__((packed))
#endif

#define BMP_HEADER_MAGIC 0x4D42 // Magic signature value used to distinguish bmp files.

typedef struct ALIGN_1 {
    uint16_t bfType;
    uint32_t bfSize;
    uint16_t bfReserved1;
    uint16_t bfReserved2;
    uint32_t bfOffBits;
} BitmapFileHeader;

#if defined(_MSC_VER)
#pragma pack(pop)
#endif

typedef struct {
    uint32_t ciexyzX;
    uint32_t ciexyzY;
    uint32_t ciexyzZ;
} CieXYZ;

typedef struct {
    CieXYZ ciexyzRed;
    CieXYZ ciexyzGreen;
    CieXYZ ciexyzBlue;
} CieXYZTriple;

typedef struct {
    uint32_t     bV5Size;
    uint32_t     bV5Width;
    uint32_t     bV5Height;
    uint16_t     bV5Planes;
    uint16_t     bV5BitCount;
    uint32_t     bV5Compression;
    uint32_t     bV5SizeImage;
    uint32_t     bV5XPelsPerMeter;
    uint32_t     bV5YPelsPerMeter;
    uint32_t     bV5ClrUsed;
    uint32_t     bV5ClrImportant;
    uint32_t     bV5RedMask;
    uint32_t     bV5GreenMask;
    uint32_t     bV5BlueMask;
    uint32_t     bV5AlphaMask;
    uint32_t     bV5CSType;
    CieXYZTriple bV5Endpoints;
    uint32_t     bV5GammaRed;
    uint32_t     bV5GammaGreen;
    uint32_t     bV5GammaBlue;
    uint32_t     bV5Intent;
    uint32_t     bV5ProfileData;
    uint32_t     bV5ProfileSize;
    uint32_t     bV5Reserved;
} BitmapV5Header;

typedef struct {
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} BitmapInfoHeader;

#define BI_RGB        0L
#define BI_RLE8       1L
#define BI_RLE4       2L
#define BI_BITFIELDS  3L
#define BI_JPEG       4L
#define BI_PNG        5L

bool ReadBMPImage(FILE* file, Image* image) {
    assert(file);
    assert(image);

    fseek(file, 0, SEEK_END);
    size_t file_size = ftell(file);
    fseek(file, 0, SEEK_SET);

    BitmapFileHeader file_header;
    ZeroMemory(file_header);

    if (file_size < sizeof(file_header)) return false; // File is smaller than the file header.
    fread(&file_header, sizeof(file_header), 1, file);


    if (file_header.bfType != BMP_HEADER_MAGIC) return false; // Magic value mismatch. File is not BMP.
    if (file_header.bfSize != file_size)        return false; // File size mismatch. File or its header is corrupted.

    // Read headers up to v5 (latest one).
    BitmapV5Header image_header;
    ZeroMemory(image_header);


    fread(&image_header, sizeof(image_header.bV5Size), 1, file);
    if (image_header.bV5Size < sizeof(uint32_t)) return false; // Header is too small. File is corrupted.

    size_t header_remainder_size = image_header.bV5Size - sizeof(image_header.bV5Size);
    fread(&image_header.bV5Width, header_remainder_size, 1, file);

    if (image_header.bV5Compression != BI_RGB) return false; // We do not support compressed images.

    size_t width  = (size_t)image_header.bV5Width;
    size_t height = (size_t)image_header.bV5Height;
    size_t image_size = width * height * 3;

    uint8_t* pixels = (uint8_t*)malloc(image_size);
    if (!pixels) return false; // Out of memory.

    fseek(file, file_header.bfOffBits, SEEK_SET);
    
    size_t line_size = width * 3;
    size_t remainder = line_size % 4;

    if (remainder) {
        int32_t padding_size = 4 - (int32_t)remainder;

        for (size_t line_index = 0; line_index < height; line_index += 1) {
            fread(pixels + line_size * line_index, line_size, 1, file);
            fseek(file, padding_size, SEEK_CUR);
        }
    } else {
        fread(pixels, image_size, 1, file);
    }

    image->width        = (uint32_t)width;
    image->height       = (uint32_t)height;
    image->pixel_format = PF_R8G8B8;
    image->pixels       = pixels;

    return true;
}

bool WriteBMPImage(FILE* file, Image* image) {
    if (image->pixel_format != PF_R8G8B8) return false; // Unsupported pixel format.

    size_t line_size = (size_t)image->width * 3;
    size_t remainder = line_size % 4;

    size_t padded_line_size = line_size;
    if (remainder) padded_line_size += 4 - (int32_t)remainder;

    size_t image_size   = padded_line_size * image->height * 3;
    size_t headers_size = sizeof(BitmapFileHeader) + sizeof(BitmapInfoHeader);
    size_t file_size    = headers_size + image_size;

    BitmapFileHeader file_header;
    BitmapInfoHeader image_header;
    ZeroMemory(file_header);
    ZeroMemory(image_header);


    file_header.bfType      = BMP_HEADER_MAGIC;
    file_header.bfSize      = (uint32_t)file_size;
    file_header.bfReserved1 = 0;
    file_header.bfReserved2 = 0;
    file_header.bfOffBits   = (uint32_t)headers_size;


    image_header.biSize        = sizeof(image_header);
    image_header.biWidth       = image->width;
    image_header.biHeight      = image->height;
    image_header.biCompression = BI_RGB;
    image_header.biSizeImage   = (uint32_t)image_size;
    image_header.biPlanes      = 1;
    image_header.biBitCount    = 24;

    fwrite(&file_header,  sizeof(file_header),  1, file);
    fwrite(&image_header, sizeof(image_header), 1, file);

    if (remainder) {
        uint8_t* pixels = (uint8_t*)image->pixels;
        int32_t padding_size = 4 - (int32_t)remainder;

        for (size_t line_index = 0; line_index < image->height; line_index += 1) {
            fwrite(pixels + line_size * line_index, line_size, 1, file);

            uint64_t padding_to_write = 0;
            fwrite(&padding_to_write, padding_size, 1, file);
        }
    } else {
        fwrite(image->pixels, line_size, image->height, file);
    }

    return true;
}
