#define _CRT_SECURE_NO_WARNINGS
#include "BmpReadWrite.h"
#include "ImageRotate.h"
#include <stdio.h>

int main(int argc, char** argv) {
    if (argc < 3) return -1;

    FILE* file_0 = fopen(argv[1], "rb");
    FILE* file_1 = fopen(argv[2], "wb");

    if (!file_0) return -1;
    if (!file_1) return -1;

    Image image_0;
    ReadBMPImage(file_0, &image_0);

    Image image_1;
    RotateImage(&image_0, &image_1);

    WriteBMPImage(file_1, &image_1);

    fclose(file_0);
    fclose(file_1);

    FreeImage(&image_0);
    FreeImage(&image_1);
    
    return 0;
}
