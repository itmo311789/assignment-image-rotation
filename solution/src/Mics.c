#include "Misc.h"

void MemsetSuperSafe(void* dest, int value, size_t size) {
	uint8_t* d = (uint8_t*)dest;
	for (size_t i = 0; i < size; i += 1) *(d + i) = value;
}

void MemcpySuperSafe(void* dest, void* src, size_t copy_range) {
	uint8_t* d = (uint8_t*)dest;
	uint8_t* s = (uint8_t*)src;

	for (size_t i = 0; i < copy_range; i += 1) *(d + i) = *(s + i);
}
