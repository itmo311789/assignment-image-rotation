#include "Misc.h"
#include "Image.h"


size_t GetPixelFormatSize(PixelFormat pixel_format) {
	static const size_t pixel_format_sizes[PF_COUNT] = {
		3, // PF_R8G8B8
	};

	assert(pixel_format < PF_COUNT && pixel_format > PF_NONE);

	return pixel_format_sizes[pixel_format - 1];
}

size_t CalculateImageSize(const Image* image) {
	assert(image);
	return (size_t)image->height * (size_t)image->width * GetPixelFormatSize(image->pixel_format);
}

void FreeImage(Image* image) {
	assert(image);
	free(image->pixels);
	ZeroMemory(*image);
}
