#include "Misc.h"
#include "ImageRotate.h"


void RotateImage(const Image* in_image, Image* out_rotated_image) {
	assert(in_image);
	assert(out_rotated_image);

	out_rotated_image->width  = in_image->height;
	out_rotated_image->height = in_image->width;
	out_rotated_image->pixel_format = in_image->pixel_format;


	size_t pixel_size = GetPixelFormatSize(in_image->pixel_format);
	size_t image_size = CalculateImageSize(in_image);

	uint8_t* src_pixels = (uint8_t*)in_image->pixels;
	uint8_t* dst_pixels = (uint8_t*)malloc(image_size);
	if (!dst_pixels) return; // Out of memory.

	out_rotated_image->pixels = dst_pixels;

	for (size_t i = 0; i < in_image->width; i += 1) {
		for (size_t j = 0; j < in_image->height; j += 1) {
			// dst[i][j] = src[j][i]

			uint8_t* src_pixel = src_pixels + ((j + 0) * in_image->width  - 0 + i) * pixel_size;
			uint8_t* dst_pixel = dst_pixels + ((i + 1) * in_image->height - 1 - j) * pixel_size;

			memcpy_s(dst_pixel, pixel_size, src_pixel, pixel_size);
		}
	}
}
